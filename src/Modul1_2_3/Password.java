package Modul1_2_3;

public class Password {

	public boolean checkPassword(String pass) {
		return checkLength(pass) && checkForOnlyDigitsAndLetters(pass) && checkThatContainsAtLeastThreeDigits(pass);
	}

	private boolean checkLength(String pass) {
		return pass.length()>=10;
	}

	private boolean checkForOnlyDigitsAndLetters(String pass) {
		return pass.matches("^[a-zA-Z0-9]*$" );
	}
	private boolean checkThatContainsAtLeastThreeDigits(String pass) {
		return pass.matches("(?:\\D*\\d){3,}.*");
	}

}
