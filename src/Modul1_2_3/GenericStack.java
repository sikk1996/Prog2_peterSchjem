package Modul1_2_3;

public class GenericStack<E> {
	private E[] list = (E[]) new Object[100];
	private int usedSize=0;

	public int getSize() {
		return usedSize;
	}
	public E peek() {
		if (isEmpty()) return null;
		return list[usedSize-1];
	}
	public void push(E o) {
		if (list.length==usedSize+1){
			E[] tmpList = (E[]) new Object[list.length*2];
			System.arraycopy(list,0,tmpList,0,list.length);
			list=tmpList;
		}
		list[usedSize++]= o;
	}
	public E pop() {
		if(isEmpty()){
			return null;
		}
		E o = list[--usedSize];
		list[usedSize]=null;
		return o;
	}
	public boolean isEmpty() {
		return (usedSize==0);
	}
	@Override
	public String toString() {
		return "stack: " + printArray();
}

	private String printArray() {
		String stringList="";
		for(E item: list){
			stringList+="'"+item.toString()+"', ";
		}
		return stringList;
	}
}
