package Modul1_2_3;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CombiningCollections {
	public static void main(String[] args) {
		Map<String, List<Integer>> testScores = new HashMap<String,List<Integer>>();


		List<Integer> joeScores = new ArrayList<Integer>();
		joeScores.add(80);
		joeScores.add(60);
		joeScores.add(97);
		joeScores.add(92);
		testScores.put("Joe",joeScores);

		List<Integer> jamieScores = new ArrayList<Integer>();
		jamieScores.add(43);
		jamieScores.add(34);
		jamieScores.add(65);
		jamieScores.add(78);
		testScores.put("Jamie",jamieScores);

		List<Integer> peterScores = new ArrayList<Integer>();
		peterScores.add(98);
		peterScores.add(91);
		peterScores.add(85);
		peterScores.add(99);
		testScores.put("Peter",peterScores);



		printScores("Joe",testScores);
	}

	public static void printScores(String studentName,Map <String, List<Integer>> scoresMap){

		List<Integer> scores = scoresMap.get(studentName);
		for (int score : scores){
			System.out.println(score);
		}

	}
}
