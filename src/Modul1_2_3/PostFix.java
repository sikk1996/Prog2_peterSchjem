package Modul1_2_3;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Scanner;

public class PostFix {


	public static int evualuateExpression(String expression){
		Scanner tokens = new Scanner(expression);
		Deque<Integer> deque = new ArrayDeque<>();

		while (tokens.hasNext()){
			if (tokens.hasNextInt())deque.push(tokens.nextInt());
			else scanAndProcessTokens(deque,tokens);
		}
		if (deque.isEmpty()) return 0;
		return deque.pop();

	}
	private static void scanAndProcessTokens(Deque<Integer> deque,Scanner tokens) {
		int sum=0;
		String operator = tokens.next();
		switch(operator){
			case "+":
				//+
				sum = deque.removeLast();
				while (!deque.isEmpty()){
					sum += deque.pop();
				}
				deque.push(sum);
				break;
			case "-":
				//-
				sum = deque.removeLast();
				while (!deque.isEmpty()){
					sum -= deque.pop();
				}
				deque.push(sum);
				break;
			case "/":
				// /
				sum = deque.removeLast();
				while (!deque.isEmpty()){
					sum /= deque.pop();
				}
				deque.push(sum);
				break;
			case "*":
				//*
				sum = deque.removeLast();
				while (!deque.isEmpty()){
					sum *= deque.pop();
				}
				deque.push(sum);
				break;
			case "%":
				//% modulo
				sum = deque.removeLast();
				while (!deque.isEmpty()){
					sum %= deque.pop();
				}
				deque.push(sum);
				break;
			default:
				//illegal sign
				while(!deque.isEmpty()) deque.pop();
				break;

		}
	}
}
