package Modul1_2_3;

import java.util.LinkedList;
import java.util.Queue;

public class Queues {
	public static void main(String[] args) {
		Queue<String> myQueue = new LinkedList<String>();

		myQueue.offer("a"); //wont get exception if queue is full, .add would get exception
		myQueue.offer("b");
		myQueue.offer("d");
		myQueue.offer("c");

		while(myQueue.peek() != null){ //.peek basically hasNext
			System.out.println(myQueue.poll());
		}
	}
}
