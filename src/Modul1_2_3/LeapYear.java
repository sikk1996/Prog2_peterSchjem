package Modul1_2_3;

public class LeapYear {

	public boolean isLeapyear(int year) {

		return ((year % 4 == 0) && (year % 100 != 0) || (year % 400 == 0));

	}

}