package Modul4;

public class GenericMergeSort {

	public <E extends Comparable<E>> E[] mergeSort(E[] list) {
		int middle = (list.length) / 2;
		int i;
		if (list.length > 1) {

			//Create temp lists and copy data into each half
			E[] left = (E[]) new Comparable[middle];
			E[] right = (E[]) new Comparable[list.length - middle];
			for (i = 0; i < middle; i++) {
				left[i] = list[i];
			}
			for (i = middle; i < list.length; i++)
				right[i - middle] = list[i];

			//Lets go
			mergeSort(left);
			mergeSort(right);
			try {
				return merge(left, right);
			} catch (NullPointerException e) {
				return list;
			}
		} else return list; //Reached the recursive bottom
	}


	public <E extends Comparable<E>> E[] merge(E[] left, E[] right) throws NullPointerException {
		E[] tempList = (E[]) new Comparable[left.length + right.length];
		int left_index = 0;
		int right_index = 0;
		int tempList_index = 0;

		//Gjenta inntil alle elementer i left og right er gjennomgått
		while (left_index + 1 <= left.length || right_index + 1 <= right.length) {

			//Kjør dersom det er mer igjen i både LEFT og Right
			if (left_index + 1 <= left.length && right_index + 1 <= right.length) {
				if (left[left_index].compareTo(right[right_index]) <= 0.0) {
					//left er minst
					tempList[tempList_index++] = left[left_index++];
				} else {
					//right er minst
					tempList[tempList_index++] = right[right_index++];
				}
			} else if (left_index + 1 <= left.length) {
				//Det er kun elementer igjen i left
				tempList[tempList_index++] = left[left_index++];
			} else if (right_index + 1 <= right.length) {
				//Det er kun elementer igjen i right
				tempList[tempList_index++] = right[right_index++];
			}

		}
		return tempList; //return merged list.
	}
}



