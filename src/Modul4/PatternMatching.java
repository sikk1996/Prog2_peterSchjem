package Modul4;

import java.util.Scanner;

public class PatternMatching {
	public static void main(String[] args) {
		PatternMatching pm = new PatternMatching();
		Scanner input = new Scanner(System.in);
		System.out.println("Type in 2 strings, den andre en en substring av den første:");
		String string1 = input.nextLine();
		String string2 = input.nextLine();

		if (pm.subStringFinder(string1,string2)){
			System.out.println("It's substring!");
		} else {
			System.out.println("It's NOT a substring");
		}

	}

	public boolean subStringFinder(String string1, String string2){
		int i;
		int e = 0;

		if (string2.length()==0) return true;
		if (string1.length()==0) return false;
		if (string2.length()>string1.length())return false;

		for(i=0;i<string1.length();i++){
			if (string1.charAt(i)==string2.charAt(e))
				e++;
			else e=0;

			if (e==string2.length())
				return true;
		}
		return false;
	}
}
