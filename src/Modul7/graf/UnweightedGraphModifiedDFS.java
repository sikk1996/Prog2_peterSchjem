package Modul7.graf;

import java.util.*;
/*written by peter*/

public class UnweightedGraphModifiedDFS<V> extends UnweightedGraph<V> {

	public UnweightedGraphModifiedDFS(V[]/*written by peter*/vertices, int[][] edges) {
		super(vertices,edges);
	}


	/** Obtain a DFS tree starting from vertex v */
	@Override
	public SearchTree dfs(int v) {
		//Temps stack
		Stack<Integer> stack = new Stack<>();

		//return variables
		int root = v;
		List<Integer> searchOrder = new ArrayList<>();
		int[] parent = new int[vertices.size()];

		//Default variable values
		for (int i = 0; i < parent.length; i++) parent[i] = -1; // Initialize parent[i] to -1
		boolean[] isVisited = new boolean[vertices.size()];

		//GO GO GO
		stack.push(root);
		while (!stack.isEmpty()) {
			v = stack.pop();
			if (isVisited[v])continue; //if vertice already visited: skip adding.
			searchOrder.add(v);
			isVisited[v]=true;

			for (Edge e: neighbors.get(v)){
				if (!isVisited[e.getV()]){ //if vertice already visited: skip stacking
					parent[e.getV()]=v;
					stack.push(e.getV());
				}
			}
		}
		return new SearchTree(root, parent,searchOrder);/*written by peter*/
	}


}/*written by peter*/
