package Modul7;

import java.util.Scanner;

public class HashForString {

	public static void main(String[] args) {
		while (true){
			Scanner input = new Scanner(System.in);
			System.out.println("Skriv inn en tekst:");
			System.out.println("Hash: "+hashCodeForString(input.nextLine()));
		}
	}
	static final int B = 31;
	public static long hashCodeForString(String string){
		long hash = 0;
		for (char s : string.substring(0,string.length()).toCharArray()) hash = B * hash + s;
		return hash;
	}
}
