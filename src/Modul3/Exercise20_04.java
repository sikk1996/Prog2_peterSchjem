package Modul3;

public class Exercise20_04 {

  // Each row in points represents a point
  private double[][] points;

  /** Define a class for a point with x- and y- coordinates */
  static class Point implements Comparable<Point> {
    double x;
    double y;

    Point(double x, double y) {
      this.x = x;
      this.y = y;
    }

    @Override
    public int compareTo(Point p2) {
      // Implement this

	    if (this.x < p2.x) //this x mindre enn p2 x
		    return -1;
	    else if (this.x == p2.x) { //this x == p2 x -> sjekk this y og p2 y
		    if (this.y < p2.y)
			    return -1;
		    else if (this.y == p2.y)
			    return 0;
		    else
			    return 1;
	    } else// this x større enn p2 x
		    return 1;

    }
    
    @Override
    public String toString() {
      return "(" + x + ", " + y + ")";
    }
  }

  static class CompareY implements java.util.Comparator<Point> {
    public int compare(Point p1, Point p2) {
      // Implement this
	    if (p1.y < p2.y)
		    return -1;
	    else if (p1.y == p2.y) {
		    if (p1.x < p2.x)
			    return -1;
		    else if (p1.x == p2.x)
			    return 0;
		    else
			    return 1;
	    } else
		    return 1;
    }
  }
}
