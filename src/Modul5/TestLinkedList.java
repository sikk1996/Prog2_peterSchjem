package Modul5;

public class TestLinkedList<E> {
	Node head;
	Node tail;
	int size=0;


	public void add(int index, E element){
		if (index==0)addFirst(element);
		else if (index >=size) addLast(element);
		else {
			Node <E> current = head;
			for (int i=1;i<index;i++)
				current=current.next;
			Node <E> temp = current.next;
			current.next=new Node<>(element);
			(current.next).next = temp;
			size++;
		}
	}

	public boolean isEmpty(){
		return (tail==null);
	}
	public void addFirst (E element){
		Node <E> newNode = new Node<>(element);
		newNode.next=head;
		head=newNode;
		size++;
		if (isEmpty()) tail = head;
	}

	public void addLast (E element){
		if (isEmpty()) head = tail = new Node<>(element);
		else {
			tail.next=new Node<>(element);
			tail=tail.next;
		}
		size++;
	}
	public E removeFirst(){
		if (head==null)return null;
		else {
			Node<E> temp = head;
			head =head.next;
			size--;
			if(head==null)tail=null;
			return temp.element;
		}
	}
	public E removeLast(){
		if (size==0)return null;
		else if (size==1){
			return removeFirst();
		} else {

			Node<E> current = head;
			for (int i=1;i<size-2;i++)
				current=current.next;
			Node<E> temp = tail;
			tail = current;
			tail.next=null;
			size--;
			return temp.element;
		}
	}
	public E remove(int index){
		if (index < 0 || index >= size || size==0)return  null;
		else if (size==1 || index==0)return removeFirst();
		else if (size==2 && index==size-1)return removeLast();
		else {
			Node<E> previous = head;
			for (int i=1;i<index;i++)
				previous=previous.next;
			Node<E>current = previous.next;
			previous.next=current.next;
			size--;
			return current.element;
		}

	}

	@Override
	public String toString() {
		String retur ="";
		int i=0;
		Node <E> current = head;
		while (current != null) {
			retur += i++ +": "+current.element+". ";
			current = current.next;
		}
		return retur;
	}
}
