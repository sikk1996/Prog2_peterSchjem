package Modul6;

import java.util.ArrayList;
import java.util.LinkedList;

public class BinarySearchTree<E extends Comparable<E>> extends AbstractTree<E> {
  /** Root node for the tree. */
  private TreeNode<E> root;

  /** Size of the tree, number of nodes. */
  private int size = 0;

  /** Create a default binary tree. */
  BinarySearchTree() {
  }

  /**
   * Create a binary tree from an array of objects.
   *
   * @param objects Array of objects to use in creation of tree.
   */
  BinarySearchTree(E[] objects) {
    for (E object : objects) {
      insert(object);
    }
  }

  /////////////////////////////////////////////////////////////////////////////
  //                            Interface Methods                            //
  /////////////////////////////////////////////////////////////////////////////

  public boolean search(E element) {
    TreeNode<E> current = root; // Start from the root

    while (current != null) {
      if (element.compareTo(current.element) < 0) {
        current = current.left;
      } else if (element.compareTo(current.element) > 0) {
        current = current.right;
      } else { // element matches current.element
        return true; // Element is found
      }
    }

    return false;
  }

  public boolean insert(E element) {
    if (root == null) { // First element in Tree.
      root = createNewNode(element);
    } else {
      // Locate the parent node
      TreeNode<E> parent = null;
      TreeNode<E> current = root;

      while (current != null) { // Go through the tree till we reach appropriate leaf node.
        parent = current;

        if (element.compareTo(current.element) < 0) {
          current = current.left;
        } else if (element.compareTo(current.element) > 0) {
          current = current.right;
        } else {
          return false; // Duplicate node not inserted
        }
      }

      // Create the new node and attach it to the parent node
      if (element.compareTo(parent.element) < 0) {
        parent.left = createNewNode(element);
      } else {
        parent.right = createNewNode(element);
      }
    }

    size++;
    return true; // Element inserted
  }

  public boolean delete(E element) {
    if (! search(element)) {
      return false; // Element is not in the tree
    }

    // Locate the node to be deleted and also locate its parent node
    TreeNode<E> parent = null;
    TreeNode<E> current = root;

    while (current != null) {
      parent = current;

      if (element.compareTo(current.element) < 0) {
        current = current.left;
      } else if (element.compareTo(current.element) > 0) {
        current = current.right;
      } else {
        break; // Element is in the tree pointed by current
      }
    }

    if (current.left == null) {
      // Case 1: current has no left children
      if (parent == null) { // We delete root, simply replace root with right child
        root = current.right;
      } else { // Connect the parent with the right child
        if (element.compareTo(parent.element) < 0) {
          parent.left = current.right;
        } else {
          parent.right = current.right;
        }
      }
    } else {
      // Case 2: The current node has a left child
      // Locate the rightmost node in the left subtree of
      // the current node and also its parent
      TreeNode<E> parentOfRightMost = current;
      TreeNode<E> rightMost = current.left;

      while (rightMost.right != null) {
        parentOfRightMost = rightMost;
        rightMost = rightMost.right; // Keep going to the right
      }

      // Replace the element in current by the element in rightMost
      current.element = rightMost.element;

      // Eliminate rightmost node
      if (parentOfRightMost.right == rightMost) {
        parentOfRightMost.right = rightMost.left;
      } else { // Special case: parentOfRightMost == current
        parentOfRightMost.left = rightMost.left;
      }
    }

    size--;
    return true; // Element removed
  }

  public void inOrder() {
    traverse(root, Traversal.IN_ORDER);
  }

  public void postOrder() {
    traverse(root, Traversal.POST_ORDER);
  }

  public void preOrder() {
    traverse(root, Traversal.PRE_ORDER);
  }

  public java.util.Iterator<E> iterator() {
    return new InOrderIterator();
  }

  public int getSize() {
    return size;
  }

  /////////////////////////////////////////////////////////////////////////////
  //                           Additional Methods                            //
  /////////////////////////////////////////////////////////////////////////////

  /** Remove all elements from the tree */
  void clear() {
    root = null;
    size = 0;
  }

  /**
   * Returns the root of the tree.
   *
   * @return Root node of the tree.
   */
  private TreeNode<E> getRoot() {
    return root;
  }

  /**
   * Creates an array list of nodes from root to the given element. List
   * will be empty if the element is not in the tree.
   *
   * @param element Element to which path should be created.
   *
   * @return List of nodes from root to element.
   */
  ArrayList<TreeNode<E>> path(E element) {
    if (! search(element)) {
      return new ArrayList<>();
    }

    ArrayList<TreeNode<E>> list = new ArrayList<>();
    TreeNode<E> current = getRoot(); // Start from the root

    while (current != null) {
      list.add(current); // Add the node to the list
      if (element.compareTo(current.element) < 0) {
        current = current.left;
      } else if (element.compareTo(current.element) > 0) {
        current = current.right;
      } else {
        break;
      }
    }

    return list; // Return an array of nodes
  }

  /**
   * Return the height of this binary tree. Height is the number of the nodes in
   * the longest path of the tree.
   *
   * @return Height og tree.
   */
  int height() {
    //TODO Implement this method
    
    return 0;
  }

  /**
   * Method for traversing the tree and displaying its nodes in a Depth First
   * manner.
   */
  void depthFirstTraversal() {
    // First add the root to queue
    // While queue is not empty
    //     Pop last element from queue.
    //     Print the popped element.
    //     Add its right and left children to stack.

    //TODO Implement this method
  }

  /**
   * Method for traversing the tree and displaying its nodes in a Breadth First
   * manner.
   */
  void breadthFirstTraversal() {
    // First add the root to queue
    // While queue is not empty
    //     Pop first element from queue.
    //     Print the popped element.
    //     Add its right and left children to stack.

    //TODO Implement this method
  }

  /////////////////////////////////////////////////////////////////////////////
  //                             Helper Methods                              //
  /////////////////////////////////////////////////////////////////////////////

  /**
   * Helper method to create a TreeNode for a given element.
   *
   * @param element Element to be put into the TreeNode
   *
   * @return A TreeNode with specified element inside it
   */
  private TreeNode<E> createNewNode(E element) {
    return new TreeNode<>(element);
  }

  /**
   * Helper method for general traversal from a subtree.
   *
   * @param node Which node to start traversal from.
   * @param mode Traversal mode, In-oder, Pre-oder or Post-order.
   */
  private void traverse(TreeNode<E> node, Traversal mode) {
    if (node == null) {
      return;
    }

    if (mode == Traversal.PRE_ORDER) {
      System.out.print(node.element + " ");
    }
    traverse(node.left, mode);
    if (mode == Traversal.IN_ORDER) {
      System.out.print(node.element + " ");
    }
    traverse(node.right, mode);
    if (mode == Traversal.POST_ORDER) {
      System.out.print(node.element + " ");
    }
  }

  /////////////////////////////////////////////////////////////////////////////
  //                             Private Classes                             //
  /////////////////////////////////////////////////////////////////////////////

  class TreeNode<T extends Comparable<T>> {
    T element;
    TreeNode<T> left;
    TreeNode<T> right;

    TreeNode(T element) {
      this.element = element;
    }
  }

  private class InOrderIterator implements java.util.Iterator<E> {
    // Store the elements in a list
    private ArrayList<E> list = new ArrayList<>();

    private int current = 0; // Point to the current element in list

    InOrderIterator() {
      inOrder(); // Traverse binary tree and store elements in list
    }

    /** In-order traversal from the root */
    private void inOrder() {
      inOrder(getRoot());
    }

    /** In-order traversal from a subtree */
    private void inOrder(TreeNode<E> root) {
      if (root == null) {
        return;
      }
      inOrder(root.left);
      list.add(root.element);
      inOrder(root.right);
    }

    /** Next element for traversing? */
    public boolean hasNext() {
      return current < list.size();
    }

    /** Get the current element and move cursor to the next. */
    public E next() {
      return list.get(current++);
    }

    /** Remove the current element and refresh the list. */
    public void remove() {
      delete(list.get(current)); // Delete the current element
      list.clear(); // Clear the list
      inOrder(); // Rebuild the list
    }
  }

  private enum Traversal {
    IN_ORDER, POST_ORDER, PRE_ORDER
  }
}
