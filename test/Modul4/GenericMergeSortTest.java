package Modul4;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.*;

public class GenericMergeSortTest {

	GenericMergeSort gms;
	@Before
	public void setUp() throws Exception {
		gms = new GenericMergeSort();
	}

	@Test
	public void emptyDoubleArray() {
		Double[] doubleArr = new Double[5];
		assertEquals(doubleArr,gms.mergeSort(doubleArr));
	}
	@Test
	public void doubleArray() {
		Double[] doubleArr = {1.1,9.9,4.4,6.3,2.9,5.6,5.7,5.5};
		Arrays.sort(doubleArr);
		assertEquals(doubleArr,gms.mergeSort(doubleArr));
	}
	@Test
	public void integerArray() {
		Integer[] intArr = {7,9,2,8,5,3,8,5,3,7,5,4,2,7,8,5,3,2};
		Arrays.sort(intArr);
		assertEquals(intArr,gms.mergeSort(intArr));
	}
	@Test
	public void integerArrayAntalletErOddetall() {
		Integer[] intArr = {7,9,2,8,5,3,8,5,3,7,5,4,2,7,8,5,3};
		Arrays.sort(intArr);
		assertEquals(intArr,gms.mergeSort(intArr));
	}
	@Test
	public void integerArray2Slots() {
		Integer[] intArr = {7,9};
		Arrays.sort(intArr);
		assertEquals(intArr,gms.mergeSort(intArr));
	}
	@Test
	public void stringArray() {
		String[] stringArr = {"A","X","B","1","2","3","C","G"};
		Arrays.sort(stringArr);
		assertEquals(stringArr,gms.mergeSort(stringArr));
	}


}