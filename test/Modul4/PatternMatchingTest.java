package Modul4;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class PatternMatchingTest {
	PatternMatching pm;
	@Before
	public void doFirst(){
		pm = new PatternMatching();
	}

	@Test
	public void CowOnTheFarm() {
		assertEquals(true,pm.subStringFinder("a large cobweb with cows on the farm","cow"));
	}
	@Test
	public void kuKosten() {
		assertEquals(true,pm.subStringFinder("eg elskar kukost","ku"));
	}
	@Test
	public void kuKosten2() {
		assertEquals(true,pm.subStringFinder("eg elskar kukost","kuk"));
	}
	@Test
	public void kuKosten3() {
		assertEquals(true,pm.subStringFinder("eg elskar kukost","ost"));
	}
	@Test
	public void kuKostenNOPE() {
		assertEquals(false,pm.subStringFinder("eg elskar kukost","jeg"));
	}
	@Test
	public void kuKostenEmptyString2() {
		assertEquals(true,pm.subStringFinder("eg elskar kukost",""));
	}
	@Test
	public void kuKostenEmptyString1() {
		assertEquals(false,pm.subStringFinder("","ost"));
	}
	@Test
	public void kuKostenEmptyStringBoth() {
		assertEquals(true,pm.subStringFinder("",""));
	}
	@Test
	public void string2LongerThanString1() {
		assertEquals(false,pm.subStringFinder("hei"," heiheiheiei"));
	}


}