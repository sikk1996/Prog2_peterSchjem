package Modul7;

import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;

import static org.junit.Assert.*;

public class MyHashMapTest {
	MyMap<Integer,String> map;

	@Before
	public void setUp() {
		map = new MyHashMap<>();
	}

	@Test
	public void test_put() {
		assertEquals("a",map.put(1,"a"));
		assertEquals("1: a ",map.toString());
	}
	@Test
	public void test_getAll() {
		map.put(1,"a");
		map.put(1,"b");
		map.put(1,"c");
		map.put(1,"d");
		assertNotNull(map.getAll(1));
		assertEquals(4,map.getAll(1).size());
		assertTrue(map.getAll(1).toString().contains("a"));
		assertTrue(map.getAll(1).toString().contains("b"));
		assertTrue(map.getAll(1).toString().contains("c"));
		assertTrue(map.getAll(1).toString().contains("d"));
	}
	@Test
	public void test_remove() {
		map.put(1,"a");
		map.put(1,"b");
		map.put(1,"c");
		map.put(1,"c");
		map.put(1,"c");
		map.put(1,"c");
		map.put(1,"c");
		map.remove(1);
		assertEquals("",map.toString());
	}

}