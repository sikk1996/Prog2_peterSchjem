package Modul1_2_3;

import org.junit.Test;

import static org.junit.Assert.*;

public class PostFixTest {

	@Test
	public void additionTest() {

		assertEquals(3,PostFix.evualuateExpression("1 2 +"));
	}
	@Test
	public void subtractionTest() {

		assertEquals(5,PostFix.evualuateExpression("7 2 - "));

	}
	@Test
	public void multiplicationTest() {

		assertEquals(6,PostFix.evualuateExpression("3 2 * "));

	}
	@Test
	public void divideTest() {

		assertEquals(1,PostFix.evualuateExpression("9 9 / "));

	}
	@Test
	public void moduloTest() {
		assertEquals(1,PostFix.evualuateExpression("5 2 % "));
	}
	@Test
	public void emptyString() {
		assertEquals(0,PostFix.evualuateExpression(""));
	}
	@Test
	public void illegalTokens() {
		assertEquals(0,PostFix.evualuateExpression("5 2 :"));
	}
}