package Modul1_2_3;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CounterTest {

	Counter c;
	@Before
	public void setUp() throws Exception {
		c = new Counter();
	}

	@Test
	public void incrementWorks() {
		int before = c.count;
		c.increment();
		assertEquals(before+1,c.count);
	}

	@Test
	public void decrementWorks() {
		c.count=2;
		int before = c.count;
		c.decrement();
		assertEquals(before-1,c.count);
	}
	@Test
	public void decrementbelow0() {
		c.count =0;
		c.decrement();
		assertNotEquals(-1,c.count);
	}
	@Test
	public void increment2Much() {
		c.count=2147483647;
		c.increment();
		assertNotEquals(-2147483648,c.count);
	}
}