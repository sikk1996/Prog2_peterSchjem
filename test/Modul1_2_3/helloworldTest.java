package Modul1_2_3;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class helloworldTest {

	helloworld h;
	@Before
	public void setUp() {
		h = new helloworld();
	}


	@Test
	public void helloMethodReturnsHello() {
		assertEquals(h.hello(),"Hello");
	}
	@Test
	public void helloWithNameReturnsCorrect() {
		int i = (int)Math.random()*10;
		assertEquals(h.hello("Peter"+i),"Hello Peter"+i);
	}
}