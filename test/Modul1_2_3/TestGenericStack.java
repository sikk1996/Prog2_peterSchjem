package Modul1_2_3;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
public class TestGenericStack {

	private GenericStack gstack;

	@Before
	public void before() { gstack = new GenericStack(); }

	@Test
	public void peek() {
		gstack.push("test 1");
		gstack.push(2);
		gstack.push("test 3");
		gstack.push("test 4");
		gstack.push("test 5");
		assertEquals("test 5",gstack.peek());
	}
	@Test
	public void peek0() {
		assertEquals(null,gstack.peek());
	}
	@Test
	public void pop() {
		gstack.push("test 1");
		gstack.push(2);
		gstack.push("test 3");
		gstack.push("test 4");
		gstack.push("test 5");
		assertEquals("test 5",gstack.pop());
	}
	@Test
	public void pop0() {
		assertEquals(null,gstack.pop());
	}
	@Test
	public void getSize() {
		gstack.push("test 1");
		gstack.push(2);
		gstack.push("test 3");
		assertEquals(3,gstack.getSize());
	}
	@Test
	public void isEmpty0() {
		assertEquals(true,gstack.isEmpty());
	}
	@Test
	public void isEmptyNot() {
		gstack.push("test 1");
		assertEquals(false,gstack.isEmpty());
	}
	@Test
	public void fillListBeyond() {
		for (int i=0;i<150;i++){
			gstack.push(i);
		}
		assertEquals(150,gstack.getSize());
	}

}
