package Modul1_2_3;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class LeapYearTest {

	LeapYear leapyear;
	public int year;
	public boolean isLeapYear;

	@Before
	public void setUp() {
		leapyear = new LeapYear();
	}

	public LeapYearTest(int year, boolean isLeapYear) {
		this.year = year;
		this.isLeapYear = isLeapYear;
	}

	@Parameterized.Parameters
	public static List<Object[]> data() {
		return Arrays.asList(new Object[][]{
				{4,true},
				{2020,true},
				{1804,true},
				{2016,true},
				{0,true},
				{100,false},
				{400,true},
				{2009,false}
		});
	}
	@Test
	public void testLeapYear() {
		assertEquals(isLeapYear,isLeapYear);
	}

}