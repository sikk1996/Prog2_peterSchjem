package Modul1_2_3;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class PasswordTest {

	private Password peterpassord;

	@Before
	public void setUp() throws Exception {
		peterpassord = new Password();
	}

	@Test
	public void testPasswordLengthBelow10() {
		assertEquals(false,peterpassord.checkPassword("123456789"));
	}
	@Test
	public void testPasswordLength10() {
		assertEquals(true,peterpassord.checkPassword("1234567890"));
	}
	@Test
	public void testPasswordAbove10() {
		assertEquals(true,peterpassord.checkPassword("12345678912313213"));
	}
	@Test
	public void testPassword0Digits() {
		assertEquals(false,peterpassord.checkPassword("abcdefghjigdgfgdfg"));
	}
	@Test
	public void testPassword2Digits() {
		assertEquals(false,peterpassord.checkPassword("12abcdefghjigdgfgdfg"));
	}
	@Test
	public void testPassword3Digits() {
		assertEquals(true,peterpassord.checkPassword("123abcdefghjigdgfgdfg"));
	}
	public void testPassword4Digits() {
		assertEquals(true,peterpassord.checkPassword("1234abcdefghjigdgfgdfg"));
	}
	@Test
	public void testPasswordOnlyLettersAndDigits() {
		assertEquals(false,peterpassord.checkPassword("abc123defghi123@"));
	}
	@Test
	public void testValidPassword() {
		assertEquals(true,peterpassord.checkPassword("abc123def456"));
	}

}