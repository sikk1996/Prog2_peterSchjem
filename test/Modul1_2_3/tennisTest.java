package Modul1_2_3;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
public class tennisTest {
	private Tennis tennis;

	@Before public void beforeClass() {
		tennis = new Tennis();
	}
	@Test
	public void deuceAndP1Win() {
		tennis.point(1);
		tennis.point(2);
		tennis.point(2);
		tennis.point(1);
		tennis.point(2);
		tennis.point(1);
		tennis.point(1);
		tennis.point(1);
		assertTrue("Player 1 won",true);
	}

	@Test
	public void deuce() {
		tennis.point(1);
		tennis.point(2);
		tennis.point(1);
		tennis.point(2);
		tennis.point(2);
		tennis.point(1);
		assertTrue("Deuce!",true);
	}
	@Test
	public void p1Wins() {
		tennis.point(1);
		tennis.point(1);
		tennis.point(1);
		tennis.point(1);
		tennis.point(1);
		assertTrue("Player 1 won",true);

	}
	@Test
	public void p2Wins() {
		tennis.point(2);
		tennis.point(2);
		tennis.point(2);
		tennis.point(2);
		tennis.point(2);
		assertTrue("Player 2 won",true);

	}

}